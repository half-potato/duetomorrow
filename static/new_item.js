$(function($) {
  var slider = $("#slider").slideReveal({
    trigger: $("#trigger"),
    push: false,
    position: 'right',
    width: '30%',
  });
});

function new_item(id) {
  $("#slider").slideReveal("toggle");
  $("#selected_id")[0].value = id;
};

Main Page
Home Page
  Panels like a file system
  Feed of old ideas
  Rotating Today list that moves things that are not due tomorrow or due negative days to a different list
  Task suggester that spits out a task based on your selected mood

Browsing a tree based task system
  Limit theory style = animated tree style
    Simple tree viewers like a map also work
  Folder viewing style with multiple panels
    Kind of a like a flatter kind of tree
    Easy to code, people are used to this style
    However, the list can grow too long
      Imposing a limit on the number of connections could help, but navigating a tree using a file system is annoying
      Not conducive to a large number of items
      Need ways to label connections?
  Folder viewer with drop downs. Only 2 open at a time
    Like reddit comments. Mimic social media so it's easy and addicting to use
    Different folder viewers
Creating a new task
  Default values for fields
  Drop downs for values in the fields
  Ad lib system
  Templates for homeworks like a math assignment always due on the same day

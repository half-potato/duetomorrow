from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from auth import login_required
from db import get_db
from icecream import ic
import random

bp = Blueprint('items', __name__)

def fetch_items(author_id, parent_id):
    """Show all the posts, most recent first."""
    db = get_db()

    if parent_id is not None:
        pri_items = db.execute(
            'SELECT p.id, name, body, author_id'
            ' FROM items p JOIN user u ON p.author_id = u.id'
            ' WHERE p.author_id = ? AND p.parent_id = ? AND p.priority IS NOT NULL'
            ' ORDER BY priority DESC'
        , (author_id, parent_id)).fetchall()
        nopri_items = db.execute(
            'SELECT p.id, name, body, author_id'
            ' FROM items p JOIN user u ON p.author_id = u.id'
            ' WHERE p.author_id = ? AND p.parent_id = ? AND p.priority IS NULL'
            ' ORDER BY created DESC'
        , (author_id, parent_id)).fetchall()
        ic(nopri_items)
    else:
        pri_items = db.execute(
            'SELECT p.id, name, body, author_id'
            ' FROM items p JOIN user u ON p.author_id = u.id'
            ' WHERE p.author_id = ? AND p.parent_id IS NULL AND p.priority IS NOT NULL'
            ' ORDER BY priority DESC'
        , (author_id,)).fetchall()
        nopri_items = db.execute(
            'SELECT p.id, name, body, author_id'
            ' FROM items p JOIN user u ON p.author_id = u.id'
            ' WHERE p.author_id = ? AND p.parent_id IS NULL AND p.priority IS NULL'
            ' ORDER BY created DESC'
        , (author_id,)).fetchall()

    items = []
    for row in pri_items+nopri_items:
        item = {}
        for i in row.keys():
            item[i] = row[i]
        item["children"] = []
        items.append(item)
    return items

def fetch_items_recursively(author_id, parent_id):
    ic(author_id, parent_id)
    items = fetch_items(author_id, parent_id)
    for item in items:
        ic(item, parent_id)
        item["children"].extend(fetch_items_recursively(author_id, item["id"]))
    return items

@bp.route('/')
def index():
    return render_template('index.html')

@bp.route('/home')
@login_required
def home():
    return render_template('items/index.html', items=fetch_items_recursively(g.user['id'], None))


def get_post(id, check_author=True):
    """Get a post and its author by id.

    Checks that the id exists and optionally that the current user is
    the author.

    :param id: id of post to get
    :param check_author: require the current user to be the author
    :return: the post with author information
    :raise 404: if a post with the given id doesn't exist
    :raise 403: if the current user isn't the author
    """
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post

@bp.route('/home', methods=('GET', 'POST'))
@login_required
def create():
    name = request.form['name']
    body = request.form['body']
    if len(body) == 0:
        body = None
    parent_id = int(request.form['parent_id'])
    if parent_id == 'None':
        parent_id = None
    error = None

    if not name:
        error = 'Name is required.'

    if error is not None:
        flash(error)
    else:
        ic( name, body, g.user['id'], parent_id)
        db = get_db()
        db.execute(
            'INSERT INTO items (name, body, author_id, parent_id)'
            ' VALUES (?, ?, ?, ?)',
            (name, body, g.user['id'], parent_id)
        )
        db.commit()
        return redirect(url_for('items.home'))


@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    print("HI")
    """Update a post if the current user is the author."""
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ? WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('blog.home'))

    return render_template('blog/update.html', post=post)


@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    """Delete a post.

    Ensures that the post exists and that the logged in user is the
    author of the post.
    """
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.home'))

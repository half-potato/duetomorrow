-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS items;
DROP TABLE IF EXISTS notifications;
DROP TABLE IF EXISTS timeref;
DROP TABLE IF EXISTS recurrances;
DROP TABLE IF EXISTS tags;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE items (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  priority INTEGER,
  parent_id INTEGER, 
  author_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  name TEXT NOT NULL,
  body TEXT,
  FOREIGN KEY (author_id) REFERENCES user (id)
);

CREATE INDEX PARENT_INDEX
ON items (id, author_id, parent_id);

-- BEGIN NOTIFACTION
CREATE TABLE notifications (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  item_id INTEGER,
  ntype TEXT NOT NULL,
  FOREIGN KEY (item_id) REFERENCES items (id)
);
-- END NOTIFACTION

-- BEGIN TIME
CREATE TABLE timeref (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  item_id INTEGER,
  trigger_type TEXT NOT NULL,
  start_t TIMESTAMP,
  end_t TIMESTAMP,
  FOREIGN KEY (item_id) REFERENCES items (id)
);

CREATE TABLE recurrances (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  timeref_id INTEGER,
  recurrence TEXT NOT NULL,
  FOREIGN KEY (timeref_id) REFERENCES items (id)
);
-- END TIME

CREATE TABLE tags (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  item_id INTEGER,
  name TEXT NOT NULL,
  FOREIGN KEY (item_id) REFERENCES items (id)
);

